(function(){
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const id = urlParams.get('id')
    var myConnector = tableau.makeConnector();
    myConnector.getSchema = function(schemaCallback) {
        var cols = [
            {
                id: "province_id",
                alias:"province_id",
                dataType: tableau.dataTypeEnum.int
            },
            {
                id: "id",
                alias:"id",
                dataType: tableau.dataTypeEnum.int
            },
            {
                id: "nama",
                alias:"nama",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "responden",
                alias:"responden",
                dataType: tableau.dataTypeEnum.int
            },
            {
                id: "peserta",
                alias:"peserta",
                dataType: tableau.dataTypeEnum.int
            }
           
        ];

        var tableSchema = {
            id: "summary_kanwil_all",
            alias: "Summary Keterisian Informasi Kanwil",
            columns: cols
        };

        schemaCallback([tableSchema]);
    };
    myConnector.getData = function(table, doneCallback) {
        $.getJSON("http://siapbelajar.kemenag.go.id/api/v1/kanwil/kesiapan-madrasah/informasi-all", function(resp) {
            var obj = resp;
            tableData = [];
            for (var i = 0, len = obj.length; i < len; i++) {
                tableData.push({
                    "province_id": obj[i].province_id,
                    "id": obj[i].id,
                    "nama": obj[i].nama,
                    "responden": obj[i].responden,
                    "peserta": obj[i].peserta

                });
            }
            table.appendRows(tableData);
            doneCallback();
        });
    };
    tableau.registerConnector(myConnector);
    $(document).ready(function() {
        $("#submitButton").click(function() {
            tableau.connectionName = "Data Keterisian Informasi Sub Kanwil";
            tableau.submit();
        });
    });
})();



