(function(){
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const id = urlParams.get('id')
    var myConnector = tableau.makeConnector();
    myConnector.getSchema = function(schemaCallback) {
        var cols = [
            {
                id: "id",
                alias:"id",
                dataType: tableau.dataTypeEnum.int
            },
            {
                id: "nama",
                alias:"nama",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "responden",
                alias:"responden",
                dataType: tableau.dataTypeEnum.int
            },
            {
                id: "peserta",
                alias:"peserta",
                dataType: tableau.dataTypeEnum.int
            }
           
        ];

        var tableSchema = {
            id: "summary_pusat_all",
            alias: "Summary Keterisian Informasi Pusat",
            columns: cols
        };

        schemaCallback([tableSchema]);
    };
    myConnector.getData = function(table, doneCallback) {
        $.getJSON("http://siapbelajar.kemenag.go.id/api/v1/pusat/kesiapan-madrasah/informasi-all", function(resp) {
            var obj = resp;
            tableData = [];
            for (var i = 0, len = obj.length; i < len; i++) {
                tableData.push({
                    "id": obj[i].id,
                    "nama": obj[i].nama,
                    "responden": obj[i].responden,
                    "peserta": obj[i].peserta

                });
            }
            table.appendRows(tableData);
            doneCallback();
        });
    };
    tableau.registerConnector(myConnector);
    $(document).ready(function() {
        $("#submitButton").click(function() {
            tableau.connectionName = "Data Keterisian Informasi Pusat";
            tableau.submit();
        });
    });
})();



