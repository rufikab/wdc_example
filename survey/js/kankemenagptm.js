(function(){
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const id = urlParams.get('id')
    var myConnector = tableau.makeConnector();
    myConnector.getSchema = function(schemaCallback) {
        var cols = [
            {
                id: "city_id",
                alias:"city_id",
                dataType: tableau.dataTypeEnum.int
            },
            {
                id: "id",
                alias:"id",
                dataType: tableau.dataTypeEnum.int
            },
            {
                id: "nama",
                alias:"nama",
                dataType: tableau.dataTypeEnum.string
            },
            {
                id: "total",
                alias:"total",
                dataType: tableau.dataTypeEnum.int
            },
            {
                id: "bdr",
                alias:"bdr",
                dataType: tableau.dataTypeEnum.int
            },
            {
                id: "bl",
                alias:"bl",
                dataType: tableau.dataTypeEnum.int
            }
           
        ];

        var tableSchema = {
            id: "ptm_kemenag",
            alias: "Presentase Proses Pembelajaran",
            columns: cols
        };

        schemaCallback([tableSchema]);
    };
    myConnector.getData = function(table, doneCallback) {
        $.getJSON("http://siapbelajar.kemenag.go.id/api/v1/kankemenag/kesiapan-madrasah/status-ptm", function(resp) {
            var obj = resp;
            tableData = [];
            for (var i = 0, len = obj.length; i < len; i++) {
                tableData.push({
                    "city_id": obj[i].city_id,
                    "id": obj[i].id,
                    "nama": obj[i].nama,
                    "total": obj[i].total,
                    "bdr": obj[i].bdr,
                    "bl": obj[i].bl

                });
            }
            table.appendRows(tableData);
            doneCallback();
        });
    };
    tableau.registerConnector(myConnector);
    $(document).ready(function() {
        $("#submitButton").click(function() {
            tableau.connectionName = "Kan Kemenag PTM";
            tableau.submit();
        });
    });
})();



